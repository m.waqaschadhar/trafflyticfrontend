import { Component, OnInit } from '@angular/core';
import {AppDataService} from '../../app-data.service';
import {RequestService} from '../../request.service'
import { empty } from 'rxjs';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  constructor( private appdata : AppDataService, private requestservice : RequestService) { }

  ngOnInit() {
    this.getprojects();
  }
   projects=[];

  async getprojects(){
     this.requestservice.geprojects().subscribe(res=> this.successResponse(res),err=> this.errorResponse(err))
    return null;
  }

  successResponse(response:any){
    console.log("service resonse", response);
    if (response.length > 0 ){
    this.projects= response;
      console.log("this project array",this.projects)
    }else{
      this.appdata.routeMeTo("import");
    }
  }
  errorResponse(response:any){

  }

  getdata(id){
    console.log(id);
   this.appdata.routeMeTo("home/"+id);
  }

}
