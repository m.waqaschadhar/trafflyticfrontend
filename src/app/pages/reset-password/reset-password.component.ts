import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RequestService } from 'src/app/request.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  form:FormGroup;
  showLoader:boolean = false;
  showError:boolean = false;
  showSuccess:boolean = false;
  errorMessage:string = "";
  successMessage:string = "";
  token:string = "";
  constructor(private requestService:RequestService, private route:ActivatedRoute) { 
    this.route.queryParams.subscribe(params => {
      this.token = params['token'];
    });
  }

  ngOnInit() {
    this.init();
  }

  init(){
    this.form = new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(16)]),
      confirm: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(16)]),
    });
  }

  reset(){
    this.showLoader = true;
    this.showError = false;
    this.showSuccess = false;
    let userData = {password: this.form.value.password};
    this.requestService.reset(userData, this.token).subscribe(res => this.success(res), err => this.error(err));
  }

  success(response:any){
    console.log(response);
    this.form.reset();
    this.showSuccess = true;
    this.showLoader = false;
    this.successMessage = "Password changed successfully.";
  }

  error(error:any){
    console.log(error);
    this.errorMessage = error.error.message;
    this.showError = true;
    this.showLoader = false;
  }

}
