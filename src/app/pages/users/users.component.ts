import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialog ,MatSnackBar,MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import { RequestService  } from '../../request.service';
import { FormGroup, FormControl } from '@angular/forms';
import { AppDataService } from '../../app-data.service'; 
import { HttpErrorResponse } from '@angular/common/http';
import { ConfirmationDialogComponent, ConfirmDialogModel } from '../../modal/confirmation-dialog/confirmation-dialog.component'


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  constructor(private reqService:RequestService, private _snackBar: MatSnackBar,public dialog: MatDialog, private appdata: AppDataService) { }
  userdata:any;
  user:any;
  confirmation:string='';
  requestdata:FormGroup;
  displayedColumns: string[] = ['serial','firstname', 'lastname', 'username','contact','action'];
 
    @ViewChild(MatPaginator) paginator: MatPaginator;


  ngOnInit() {
    this.user = this.appdata.getUser();
    this.requestdata = new FormGroup({
     
      username: new FormControl(),
      contact: new FormControl(),
      password: new FormControl(),    
      
    });
    this.reqService.getClientuser().subscribe(res=>this.success(res));
  }

  success(res){
    if(res.length >0 ){
      this.userdata= res;
      this.userdata.paginator = this.paginator;
    }
    console.log("userss",res);
  }

  adduserdata(){
    let role= "client";
    let data= this.requestdata.value;
    data.role = "client";
    console.log("request Data",data);
    this.reqService.create(data).subscribe(res => this.successresp(res),err=>this.ErrorResponse(err) );

  }
  successresp(res){
    console.log("Success",res);
    this.reqService.getClientuser().subscribe(res=>this.success(res));
    this.openSnackBar("Client Added Successfuly");
  }
  ErrorResponse(err){
    console.log("Error",err)
  }

 async deleteUser(id){

    console.log("Form ID",id);
     await this.confirmDialog(id);

  }

   async confirmDialog(id){
    const message = `Are you sure you want to do this?`;

    const dialogData = new ConfirmDialogModel("Confirm Action", message);

    const dialogRef =  this.dialog.open(ConfirmationDialogComponent, {
      maxWidth: "500px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
     
      if(dialogResult==true){
        this.deleteuserbyid(id);
       
      }
     
    });

   
  }
  deleteuserbyid(id){
this.reqService.deleteuser(id).subscribe(res=> this.deletesuccess(res),err=>this.deleteerr(err))

  }

  deletesuccess(res){
    this.reqService.getClientuser().subscribe(res=>this.success(res));
    this.openSnackBar("Client Deleted Successfuly!");

  }

  openSnackBar(message: string) {
    this._snackBar.open(message, "", {
      duration: 2000,
    });
  }
  deleteerr(err){

    console.log(err)
  }

  updateUser(data){

    console.log("received data",data);
  }

}
