import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { forkJoin } from 'rxjs';
import { RequestService } from 'src/app/request.service';
import { Chart, HIGHCHARTS_MODULES } from 'angular-highcharts';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { element } from '@angular/core/src/render3/instructions';
import { Key } from 'protractor';
import { keys, time } from 'highcharts';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild('file') file;
  public files: Set<File> = new Set();
  singlevehicle = null;


  //for-testing

  directionData = [
    { y: 7, name: "Left (" + 7 + ")", color: '#28a745', legendIndex: 1 },
    { y: 13, name: "Right (" + 13 + ")", color: '#007bff', legendIndex: 2 },
    { y: 18, name: "Front (" + 18 + ")", color: '#ffc107', legendIndex: 3 }
  ];

  vehicleData = [
    { y: 90, name: "Car", color: '#007bff' },
    { y: 85, name: "Bike", color: '#28a745' },
    { y: 80, name: "Bus", color: '#ffc107' },
    { y: 75, name: "Truck", color: '#dd7bff' },
    { y: 60, name: "bicycle", color: '#28a745' },
    { y: 55, name: "Coaster", color: '#007bff' },
    { y: 54, name: "Rickshaw", color: '#ffc107' },
    { y: 30, name: "qingqi", color: '#dd7bff' }
  ];
  //for testing 
  colors = { car: "#3498DB", bicycle: "#DC7633", motorcycle: "#76D7C4", rickshaw: "#D2B4DE", qingqui: "#239B56", hiace: "#7D3C98", pickup: "#F4D03F", utility_vehicle: "#17202A", coaster: "#AED6F1", bus: "#E74C3C", tractor_trolley: "#ABEBC6", truck: "#873600", trailer: "#6C3483", animal_driven: "#808B96" }
  directionColor = { D1: "blue", D2: "brown" }
  projectid: any;
  total_count = [];
  
  total_vehicles: any;
  dir_totalCount:any;
  ProjectData: any;
  vehicle_categories:any;
  direction_categories:any;


  directional_totalcount:any;

  progress;
  analytics: any;
  locationId: number;
  directionChart: Chart;
  allvehiclechart: Chart;
  alldirectionchart: Chart;
  directionChart2: Chart;
  vehicleChart: Chart;
  timelineChart: Chart;
  barchart: Chart;
  today: Date = new Date();

  vehicles: any = [
    { text: "Bike", value: "bike" },
    { text: "Car", value: "car" },
    { text: "Bus", value: "bus" },
    { text: "Truck", value: "truck" }
  ];

  directions: any = [
    { text: "Front", value: "front" },
    { text: "Left", value: "left" },
    { text: "Right", value: "right" }
  ];

  requestData: any = {
    locationId: this.projectid,
    start: new FormControl(),
    end: new FormControl(),
    vehicle: new FormControl(),
    direction: new FormControl()
  }

  


  constructor(public dialog: MatDialog, private requestService: RequestService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      this.projectid = params.get("id");
      this.getcompleteprojdetail(this.projectid);

    });
    

   // this.vehicleChart = this.generatelineChart("Vehicle Statistics", this.vehicleData);
   // this.timelineChart = this.generatelineChart("Time Line", this.vehicleData);
    
   
  }

  getcompleteprojdetail(projectID: any) {
    this.requestService.getprojdetail(projectID).subscribe(res => this.projsuccessResponse(res), err => this.projerrResponse(err));
    
    this.requestService.geprojectsbyid(projectID).subscribe(res => this.metasuccessResponse(res), err => this.metaerrResponse(err));

  }
  metasuccessResponse(metaresponse:any){
   
    if(metaresponse!=null){
      this.direction_categories=metaresponse[0].directions;
    }
  }
  metaerrResponse(err){


  }

  async projsuccessResponse(response) {
   

    //console.log("jsonparse",);
   
    this.successResponse(JSON.parse(JSON.stringify(response)));

    let vehicle_array = []; 
    let start_time = [];
    let chartsdata = [];
    let colors = { car: "#3498DB", bicycle: "#DC7633", motorcycle: "#76D7C4", rickshaw: "#D2B4DE", qingqui: "#239B56", hiace: "#7D3C98", pickup: "#F4D03F", utility_vehicle: "#17202A", coaster: "#AED6F1", bus: "#E74C3C", tractor_trolley: "#ABEBC6", truck: "#873600", trailer: "#6C3483", animal_driven: "#808B96" }

    await response.forEach(element => {
      this.total_count.push(element.total_count);
      vehicle_array.push(element.vehicles);
      start_time.push(element.start_time);
    });
   
   
  
    let directionsumobject = await this.getdirectionsum(response);
    let directionObject = await this.getDirectionPieChartData(directionsumobject, this.directionColor);
    

    let vehsumobject = await this.getsumofarrayobject(vehicle_array);
    this.vehicle_categories= vehsumobject;
    for (let key in vehsumobject) {

      if (vehsumobject.hasOwnProperty(key)) {
        let newobj = {
          y: vehsumobject[key],
          name: key + " (" + vehsumobject[key] + ")",
          color: colors[key]
        }
        chartsdata.push(newobj);
      }

    }
   

    const total = await this.getsumofarray(this.total_count);
    this.total_vehicles=total;
    this.allvehiclechart = this.generateChart("Vehicle Classification", chartsdata, total);
    this.alldirectionchart = this.generateChart("Directional Classification", directionObject, total);
  }

  async getDirectionPieChartData(obj: any, dircolor: any) {
    console.log("colors Parameter", dircolor);
    obj.forEach(element => {
      let key = element.name
      let namearr = key.split(" (");
      let dirname = namearr[0];
      //console.log("split Result",namearr[0]);
      element.color = dircolor[dirname];
    })

    return obj;

  }

  async getdirectionsum(data) {
    let totaldirections = Array.from(new Set(data.map(item => item.direction)));
    let directions = [];

    for (var i = 0; i < totaldirections.length; i++) {
      let count = 0;

      data.forEach(element => {

        if (element.direction === totaldirections[i]) {
          count += element.total_count;
        }
      })

      let obj = { y: count, name: totaldirections[i] + " (" + count + ")" }
      directions.push(obj);
    }

    return directions;

  }
  async getsumofarray(data) {
    let total = 0;
    for (var i = 0; i < data.length; i++) {
      total += data[i];
    }
    return total;
  }

  async getsumofarrayobject(data:any) {
    let count = 0;
    let total = {};
    data.forEach(row => {
      if (count == 0) {
        total = JSON.parse(JSON.stringify(row));
      }
      else {
        for (var key in row) {
          if (row.hasOwnProperty(key)) {

            total[key] += row[key];
          }
        }
      }
      count++;
    })


    for (var propName in total) {
      if (total[propName] === null || total[propName] === undefined ) {
        delete total[propName];
      }
    }

    return total;

  }
  projerrResponse(error) {
    console.log("error", error);
  }

  getAnalytics() {
    let projectID= this.projectid;
    let startDate = this.requestData.start.value;
    let endDate = this.requestData.end.value;
    let vehicles = this.requestData.vehicle.value.join(',');
    let directions = this.requestData.direction.value.join(',');
    let requestData = { locationId: projectID, start: startDate, end: endDate, vehicle: vehicles, direction: directions };
    console.log('formData:', requestData);

   this.requestService.getAnalytics(requestData).subscribe(res => this.successResponse(res), err => this.errorResponse(err));
    //this.getTimeline(requestData);
  }

 async successResponse(resp) {
 
   
    let response = JSON.parse(JSON.stringify(resp))

    const vehiclearray=[];
    await response.forEach(element => {
      vehiclearray.push(element.vehicles); 
    });
    console.log("Response for form3",resp);
    const dynamicvehdata = await this.dynamicVehPiechart(vehiclearray);
    const dynamicdirdata = await this.dynamicdirpie(response);
    const dynamiclinedata= await this.dynamicvehline(response);
    const Dynamicbardata = await this.dynamicbarline(vehiclearray);
    console.log("data for bar",Dynamicbardata);
    this.dir_totalCount= dynamicvehdata.count;


    this.directionChart = this.generateChart("Vehicle Classification", dynamicvehdata.data, dynamicvehdata.count);
    this.directionChart2 = this.generateChart("Directional Classification", dynamicdirdata.data, dynamicvehdata.count);
    this.barchart = this.generatebarChart("Vehicle Count", Dynamicbardata.data);
    this.vehicleChart = this.generatelineChart("Vehicles Trend",dynamiclinedata.data,dynamiclinedata.count);
  }

  errorResponse(error) {

    console.log(error);
  }
 
   sum( obj ) {
    var sum = 0;
    for( var el in obj ) {
     
      if( obj.hasOwnProperty( el ) ) {
        sum += parseFloat( obj[el] );
      }
  
  }
    return sum;
  }
  async dynamicbarline(data){

    let color= this.colors;
    const chartspiedata=[];
  
    let sumObject= await this.getsumofarrayobject(data);
   
    let total = this.sum(sumObject);
  
    
    for (let key in sumObject) {

      if (sumObject.hasOwnProperty(key)) {
        let newobj = {
          y: sumObject[key],
          name: key ,
          color: color[key]
        }
        chartspiedata.push(newobj);
      }

    }
    
     chartspiedata.sort(function(a,b){
      return a.y <b.y?1:a.y >b.y?-1:0
     })



   let response= {
    count: total,
    data: chartspiedata 
   }
   return response;
  }

  async dynamicVehPiechart(data){
    let color= this.colors;
    const chartspiedata=[];
  
    let sumObject= await this.getsumofarrayobject(data);
    let total = this.sum(sumObject);
  
    
    for (let key in sumObject) {

      if (sumObject.hasOwnProperty(key)) {
        let newobj = {
          y: sumObject[key],
          name: key + " (" + sumObject[key] + ")",
          color: color[key]
        }
        chartspiedata.push(newobj);
      }

    }
   let response= {
    count: total,
    data: chartspiedata 
   }
   return response;
  }

   async dynamicvehline(response){ 
    
        let objects={}
        let timeline=[];
let resp=response;
console.log("before resp array",resp)
  resp.forEach(element=>{
        let res= element.vehicles
    for (var propName in res) {
    if (res[propName] === null || res[propName] === undefined || res[propName] === 0) {
      delete res[propName];
    }
  }
})
console.log("after resp array",resp);
        let timeseries=Array.from(new Set(response.map(item => item.timestart)));


     let totaldirections = Array.from(new Set(response.map(item => item.direction)));
     for (var i = 0; i < totaldirections.length; i++) {
       let total_number = 0;
       response.forEach(element => {

         if (element.direction === totaldirections[i]) {

           let total = this.sum(element.vehicles);
           objects["time" + total_number] = total;

           total_number++
         }
       })

           
         timeline.push(objects);  
         objects={}
          
       
         }
         if(timeline.length>1){
            let result=[];
          const sumofTimeline = await this.getsumofarrayobject(timeline);
          for (let key in sumofTimeline) {

            if (sumofTimeline.hasOwnProperty(key)) {
              let newobj = {
                y: sumofTimeline[key],
              
              }
              result.push(newobj);
            }
      
          }
         


          let resultsum ={count: timeseries, data:result}
          return resultsum;
         }else{
            let res=[];
           let single=timeline[0];
          for (let key in single) {

            if (single.hasOwnProperty(key)) {
              let newobj = {
                y: single[key],
              
              }
              res.push(newobj);
            }
      
          }
            let result={count:timeseries,data:res}
            return result;
         }
        
    }

     async dynamicdirpie(dirresponse){
     
      const dircolor = this.directionColor;
      let totaldirections = Array.from(new Set(dirresponse.map(item => item.direction)));
      const directions = [];
      let total_number=0
      let demo=[]
      

      for (var i = 0; i < totaldirections.length; i++) {
       let name:any;
        dirresponse.forEach(element => {
        
          if (element.direction === totaldirections[i]) {
           
           demo.push(element.vehicles)
         

          }
        })
        let sum= await this.getsumofarrayobject(demo);
        let total =  this.sum(sum);
        total_number += total;
          name=totaldirections[i];
        let obj= {name: totaldirections[i]+"("+total+")", y:total,color:dircolor[name] }
      
        directions.push(obj);
        demo=[];
        total=0;
        sum=0;
    
      }

      let result = {
        count:total_number,
        data:directions
      }
      return result;




  


    }


  generateChart(title, data, total) {
    let a = total;
    console.log(a)
    let seriesOptions: any = {
      type: "pie",
      name: "",
      data: data,
      innerSize: '50%',
      minSize: '100px',
      size: '230px',
      dataLabels: {
        enabled: true,
        overflow: true,
        crop: false
      },
      showInLegend: true,
      startAngle: 45
    };
    return new Chart({
      title: {
        text: title,

      },
      credits: {
        enabled: false
      },
      legend: {
        reversed: true,
        verticalAlign: 'bottom',
        floating: false,
       
        y:20,
       
    },
      series: [
        {
          type: 'pie',
          name: 'Total',
          size: '39%',
          dataLabels: {
            enabled: true,
            distance: -30,
            y: -50,
            style: {
              fontSize: "18px",
              fontFamily: "Arial, sans-serif",
              fontWeight: "normal"
            },
            labelFormatter: function () {
              return '<span style="margin-top:-30px;">' + this.name + '</span>' + '$' + this.y;
            }
          },
          data: [{
            y: a,
            name: 'Total<br>' + a,
            color: "white",

          }]
        }
        ,
        seriesOptions],
      colors: ['#28a745', '#007bff', '#ffc107']
    }


    );
  }
  generatelineChart(title, data,timeline) {
    let seriesOptions: any = {
      type: "line",
      name: "",
      data: data,
      innerSize: '50%',
      minSize: '100px',
      size: '250px',
      dataLabels: {
       // enabled: true,
       // overflow: false,
       // crop: false
      },
      startAngle: 90
    };
    return new Chart({
      title: {
        text: title
      },
      xAxis:{
        categories: timeline
      },
      plotOptions: {
        series: {
            pointStart: 0
        }
    },
      credits: {
        enabled: false
      },
      series: [seriesOptions],
      colors: ['#28a745', '#007bff', '#ffc107']
    });
  }



  generatebarChart(title, data) {
    let seriesOptions: any = {
      type: "bar",
      name: "",
      data: data,
      maxPointWidth: 30,
      pointPadding: -1,

      minSize: '100px',
      size: '250px',
      dataLabels: {
        enabled: true,

      },

    };
    return new Chart({
      title: {
        text: title
      },
      credits: {
        enabled: false
      },
      xAxis: {
        type: 'category'
      },
      yAxis: {
        min: 0,
        visible: false,
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          borderWidth: 0,

          dataLabels: {
            enabled: true,
            format: '{point.y}'
          }
        }

      },
      series: [seriesOptions],
      colors: ['#28a745', '#007bff', '#ffc107']
    });
  }

  getTimeline(requestData) {
    this.requestService.getTimeline(requestData).subscribe(res => this.timelineSuccessResponse(res), err => this.timelineErrorResponse(err));
  }


  toTime(hour: number) {
    hour = hour + 5;
    let rem = hour % 12;
    let ampm = hour / 12;
    if (ampm == 0) {
      return rem + "AM";
    } else {
      return rem + "PM";
    }
  }

  toDate(day, month) {
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Nov", "Dec"];
    return months[month - 1] + ", " + day;
  }

  timelineSuccessResponse(response) {
    console.log(response);
    let timelineData;
    if (response.length) {
      timelineData = [];
      for (let i = 0; i < response.length; i++) {
        let label = this.toTime(response[i]._id.hour) + ' ' + this.toDate(response[i]._id.day, response[i]._id.month);
        timelineData[i] = [label, response[i].total];
      }
    } else {
      timelineData = [];
    }
    console.log("Timeline Data:", timelineData);
    this.timelineChart = this.generateTimelineChart("Hourly", timelineData);
  }
  timelineErrorResponse(error) {
    console.log(error);
  }

  generateTimelineChart(title, data) {
    let seriesOptions: any = {
      chart: {
        type: 'column'
      },
      title: {
        text: title
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: "Impressions"
        }
      },
      legend: {
        enabled: false
      },
      colors: ['#007bff'],
      series: [
        {
          name: 'Impressions',
          data: data,
          pointWidth: 20,
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y}',
            y: 10,
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
            }
          }
        }
      ]
    };
    return new Chart(seriesOptions);
  }

}