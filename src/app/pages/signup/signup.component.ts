import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { RequestService } from 'src/app/request.service';
import { AppDataService } from 'src/app/app-data.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  isLinear = false;
  form: FormGroup;
  email: string = "";
  password: string = "";
  firstName:string = "";
  lastName:string = "";
  showLoader:boolean = false;
  showError:boolean = false;
  showSuccess:boolean = false;
  errorMessage:string = "";
  successMessage:string = "";
  countries;

  constructor(private _formBuilder: FormBuilder, private requestService:RequestService, private dataService:AppDataService) {
    this.countries = dataService.countries;
  }

  ngOnInit() {
    this.form = this._formBuilder.group({
      username: new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]),
      contact:  new FormControl('', [Validators.required, Validators.pattern('[0-9+-]*'), Validators.minLength(7), Validators.maxLength(15)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(16)]),
      country: new FormControl('', [Validators.required]),
    });
  }

  signup() : void {
    this.showLoader = true;
    let userData = this.form.value;
    this.requestService.create(userData).subscribe(res => this.signupSuccess(res), err => this.signupError(err));
  }

  signupSuccess(res){
    console.log(res);
    this.showSuccess = true;
    this.showLoader = false;
    this.successMessage = "Account has been created. Visit Sign in page.";
  }

  signupError(err){
    this.showError = true;
    this.showLoader = false;
    this.errorMessage = err.message;
  }

}
