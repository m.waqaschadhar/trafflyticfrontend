import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RequestService } from 'src/app/request.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {

  form:FormGroup;
  showLoader:boolean = false;
  showError:boolean = false;
  showSuccess:boolean = false;
  errorMessage:string = "";
  successMessage:string = "";
  constructor(private requestService:RequestService) { }

  ngOnInit() {
    this.init();
  }

  init(){
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')])
    });
  }

  recover(){
    this.showLoader = true;
    this.showError = false;
    this.showSuccess = false;
    let userData = this.form.value;
    this.requestService.forgot(userData).subscribe(res => this.success(res), err => this.error(err));
  }

  success(response:any){
    console.log(response);
    this.form.reset();
    this.showSuccess = true;
    this.showLoader = false;
    this.successMessage = "Check email for reset link.";
  }

  error(error:any){
    console.log(error);
    this.errorMessage = error.error.message;
    this.showError = true;
    this.showLoader = false;
  }
}
