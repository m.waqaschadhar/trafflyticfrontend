import { Component, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import { async } from 'q';
import { element, Key } from 'protractor';
import { empty } from 'rxjs';
import { RequestService } from '../../request.service';
import {AppDataService} from '../../app-data.service';
import { numberFormat } from 'highcharts';
import { MatDialog, MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent implements OnInit {

  constructor(private reqService: RequestService,private appdata: AppDataService,private _snackbar:MatSnackBar) { }
  files: any = [];
  projectId: any;
  vehiclesCatergory: any;
  projectsdetail: any;
  spinner=false;
  async uploadFile(evt: any) {

    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = async (e: any) => {
     /* read workbook */
      this.spinner= true;
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      this.projectsdetail = {};
      let directionObj = {}
      /* grab first sheet */
      const wsname = wb.SheetNames;

      for (var i = 0; i <= 1; i++) {
        const ws: XLSX.WorkSheet = wb.Sheets[wsname[i]];
        const data = XLSX.utils.sheet_to_json(ws, { header: 1 });
        this.projectsdetail[wsname[i]] = data;
      }

      if (this.projectsdetail != null) {
        let dirData = this.projectsdetail[wsname[0]];
        this.vehiclesCatergory = dirData[6]

      }

      for (let sheet in this.projectsdetail) {
        let direction = this.projectsdetail[sheet];
        directionObj[sheet] = direction[2][1];
      }
      let projectinfo = await this.getprojectinfo(this.projectsdetail["D1"]);
      console.log("project info",projectinfo)
      if (projectinfo != null) {
        projectinfo["directions"] = directionObj;
        this.reqService.saveProjectmeta(projectinfo).subscribe(res => this.successResponse(res), err => this.errorResponse(err));
      }else{ 
        this.spinner=false;
        this.openSnackBar("Data Format is not Valid");
      }
      console.log("veh array", this.vehiclesCatergory);

    };
    reader.readAsBinaryString(target.files[0]);

  }

  async readdata(id, vehicles, data) {
    let response = [];
    let starttime = [];
    let formatedstarttime = [];
    let formatedendtime = [];

    let endtime = [];
    let timearray = data["D1"];

    for (let i = 7; i < data["D1"].length - 1; i++) {
      starttime.push(timearray[i][1]);
      endtime.push(timearray[i][2]);

    }
    formatedstarttime = await this.getTime(starttime);
    formatedendtime = await this.getTime(endtime);
    console.log("Formated start time",formatedstarttime);
    console.log("Formated end time",formatedendtime);
    
    if (formatedstarttime != null && this.projectId != null) {
      for (let direction in data) {
        let dirdata = data[direction]
        for (var row = 7; row < data[direction].length - 1; row++) {
          let obj = {};
          let veh = {};
          for (var col = 1; col < vehicles.length; col++) {

            if (col >= 3 && col <= 16) {
              veh[vehicles[col]] = Math.round(dirdata[row][col]);
            } else if (col == 1 || col == 2) {
              obj["timestart"] = formatedstarttime[row - 7];
              obj["timeend"] = formatedendtime[row - 7];

            } else {
              obj[vehicles[col]] = Math.round(dirdata[row][col]);
            }
            obj["project_id"] = id;
            obj["direction"] = dirdata[2][1];
          }
          obj["vehicles"] = veh;
          response.push(obj);
        }
      }
    }

    if (response != null) {
      this.reqService.uploadproject(response).subscribe(res => this.success(res), err => this.error(err));
    }
    console.log("Formated Response", response);

  }
  success(res) {
    console.log(res);
    if (res.length > 1) {
  this.appdata.routeMeTo('home/'+this.projectId);
    }
  }
  error(err) {
    console.log(err);

  }
  openSnackBar(message: string) {
    this._snackbar.open(message, "", {
      duration: 2000,
    });
  }

  async successResponse(res) {
console.log("successResponseMeta")
    this.projectId = res;
  
    if (this.projectId != null) {
      this.readdata(this.projectId, this.vehiclesCatergory, this.projectsdetail);

    }else{
      this.spinner = false;
      this.openSnackBar("Project existed already.");
    }

  }
  errorResponse(err) { 
    console.log("Metaerror",err);
  }

  async getprojectinfo(data) {
    if(data[0][0] == "Projectname" || data[1][0] == "Location" || data[2][0] == "Direction"){
    if (data[2][1] != null || data[2][1] != empty) {

      let info = {
        projectname: data[0][1], location: data[1][1], day: data[3][1], date: data[4][1]
      }
      return info;

    } else {
      return null;
    }
  }else{ return null; }
  }
  async datasuccess(data: any) {
    return await data;
  }
  deleteAttachment(index) {
    this.files.splice(index, 1)
  }

  async getTime(start) {
    let hours;
    let minutes;
    let newarray = [];
    for (var a = 0; a < start.length; a++) {
      let hour = start[a] * 24;
      let h = hour.toString().split(".");
      if (h.length > 1) {
        hours = +h[0];
        let min = +h[1];
        minutes = min * 60;
      } else {
        hours = hour;
        minutes = "00";

      }

      let res = hours + ":" + minutes.toString().substr(0, 2);
      newarray.push(res);

    }
    return newarray;
  }


  ngOnInit() {
  }

}










