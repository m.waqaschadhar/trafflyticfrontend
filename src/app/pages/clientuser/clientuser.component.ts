//import { Component, OnInit } from '@angular/core';
import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialog ,MatSnackBar,MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import { RequestService  } from '../../request.service';
import { FormGroup, FormControl } from '@angular/forms';
import { AppDataService  } from '../../app-data.service';
import { ConfirmationDialogComponent, ConfirmDialogModel } from '../../modal/confirmation-dialog/confirmation-dialog.component'


@Component({
  selector: 'app-clientuser',
  templateUrl: './clientuser.component.html',
  styleUrls: ['./clientuser.component.scss']
})
export class ClientuserComponent implements OnInit {

  constructor(private reqService:RequestService,private _snackBar: MatSnackBar,private dialog:MatDialog,private appData: AppDataService) { }
  userdata:any;

  user:any;
  adduser:FormGroup;
  displayedColumns: string[] = ['serial','firstname', 'lastname', 'username','contact','action'];
 
    @ViewChild(MatPaginator) paginator: MatPaginator;


  ngOnInit() {
  this.user= this.appData.getUser();
    this.adduser = new FormGroup({
     
      username: new FormControl(''),
      contact: new FormControl(''),
      password: new FormControl(''),
      role: new FormControl(''),
      
    });
    this.reqService.getClientuserbyid(this.user._id).subscribe(res=>this.success(res));
  }

  success(res){
    if(res.length >0 ){
      this.userdata= res;
      this.userdata.paginator = this.paginator;
    }
    console.log("userss",res);

  }
  addclientuser(){
    let data= this.adduser.value;
    data.clientid = this.user._id;

    //console.log("client USer",data);
    this.reqService.create(data).subscribe(res=>this.successresponse(res),err=>this.erresponse(err));

  }
  successresponse(res){
    //console.log("Success",res)
    this.reqService.getClientuserbyid(this.user._id).subscribe(res=>this.success(res));
    this.openSnackBar("User created successfuly");
  }
  erresponse(err){
    console.log("")
  }

  async confirmDialog(id){
    const message = `Are you sure you want to delete this User?`;

    const dialogData = new ConfirmDialogModel("Confirm Action", message);

    const dialogRef =  this.dialog.open(ConfirmationDialogComponent, {
      maxWidth: "500px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
     
      if(dialogResult==true){
        this.deleteuserbyid(id);
       
      }
     
    });

   
  }
  deleteuserbyid(id){
this.reqService.deleteuser(id).subscribe(res=> this.deletesuccess(res),err=>this.deleteerr(err))

  }

  deletesuccess(res){
   this.reqService.getClientuserbyid(this.user._id).subscribe(res=>this.success(res));
    this.openSnackBar("User Deleted Successfuly!");

  }

  openSnackBar(message: string) {
    this._snackBar.open(message, "", {
      duration: 2000,
    });
  }
  deleteerr(err){

    console.log(err)
  }





}
