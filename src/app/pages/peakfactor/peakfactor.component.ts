import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog ,MatSnackBar,MatTableDataSource, MatSort, MatPaginator} from '@angular/material';

@Component({
  selector: 'app-peakfactor',
  templateUrl: './peakfactor.component.html',
  styleUrls: ['./peakfactor.component.scss']
})
export class PeakfactorComponent implements OnInit {

  constructor() { }
factors:any;
 

  displayedColumns: string[] = ['serial','vehicle', 'peakfactor', 'edit','action'];
 
  @ViewChild(MatPaginator) paginator: MatPaginator;


  ngOnInit() {
   
    this.factors = [
      {vehicle: 'car', peakfactor: '0.7', edit:'1' },
      {vehicle: 'bus', peakfactor: '0.8', edit:'1' },
      {vehicle: 'truck', peakfactor: '0.1', edit:'1' },
      {vehicle: 'cycle', peakfactor: '0.4', edit:'1' },
     
     ];
     this.factors.paginator = this.paginator;
  }

}
