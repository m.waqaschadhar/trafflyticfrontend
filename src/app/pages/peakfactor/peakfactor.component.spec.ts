import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeakfactorComponent } from './peakfactor.component';

describe('PeakfactorComponent', () => {
  let component: PeakfactorComponent;
  let fixture: ComponentFixture<PeakfactorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeakfactorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeakfactorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
