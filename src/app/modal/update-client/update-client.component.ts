import { Component, OnInit,Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup,FormControl } from '@angular/forms';

@Component({
  selector: 'app-update-client',
  templateUrl: './update-client.component.html',
  styleUrls: ['./update-client.component.scss']
})
export class UpdateClientComponent implements OnInit {

  clientdata:any;
  clientid:any;
  resetForm:FormGroup
  constructor(public dialogRef: MatDialogRef<UpdateClientComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 

      this.clientdata = data;
      this.clientid = data._id;
     // this.message = data.message;
    }



  ngOnInit() {
    this.resetForm = new FormGroup({
     
      contact: new FormControl(this.data.contact),
      status: new FormControl( this.data.status)
     
      
    });

  }

  UpdateClient(){
    let res = this.resetForm.value;
    
    console.log("updateformvalue",res);

  }

}
