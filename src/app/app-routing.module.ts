import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import { ForgotComponent } from './pages/forgot/forgot.component';
import { AuthGuard } from './auth.guard';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';

import { MediaFilesComponent } from './pages/media-files/media-files.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { EditProfileComponent } from './pages/profile/edit-profile/edit-profile.component';
import {ProjectsComponent} from './pages/projects/projects.component';
import {UsersComponent} from './pages/users/users.component'; 
import { ImportComponent} from './pages/import/import.component';
import { PeakfactorComponent} from './pages/peakfactor/peakfactor.component';
import { ClientuserComponent } from './pages/clientuser/clientuser.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'import', component: ImportComponent },
  { path: 'signup', component: SignupComponent},
  { path: 'forgot', component: ForgotComponent},
  { path: 'reset_password', component: ResetPasswordComponent},
  
  { path: 'home/:id', pathMatch: 'full', component:HomeComponent, canActivate : [AuthGuard]},
  { path: '', redirectTo:'projects', pathMatch:'full', canActivate : [AuthGuard]},
  { path: 'projects', component: ProjectsComponent, canActivate : [AuthGuard]},
  { path: 'profile', component: ProfileComponent, canActivate : [AuthGuard] },
  { path: 'users', component: UsersComponent, canActivate : [AuthGuard] },
  { path: 'clientusers', component: ClientuserComponent, canActivate : [AuthGuard] },
  { path: 'factors', component: PeakfactorComponent, canActivate : [AuthGuard] },
  { path: 'profile/edit', component: EditProfileComponent, canActivate : [AuthGuard] }, 
  { path: 'media', component: MediaFilesComponent, canActivate : [AuthGuard] },
  { path: '404', component: NotFoundComponent, canActivate : [AuthGuard]},
  { path: '**', redirectTo: '404', canActivate : [AuthGuard]},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes) 
  ],
  exports:[
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
