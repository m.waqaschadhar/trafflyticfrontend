import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import {DataTableModule} from "angular-6-datatable";
import * as more from 'highcharts/highcharts-more.src';
import * as exporting from 'highcharts/modules/exporting.src';
import {} from '@angular/material/expansion';
import {MatTableModule} from '@angular/material/table';

import {MatToolbarModule,MatTooltipModule,MatSortModule,MatExpansionModule, MatPaginatorModule, MatFormFieldModule, MatOptionModule, MatSelectModule, MatInputModule, MatIconModule, MatSidenavModule, MatListModule, MatButtonModule, MatMenuModule, MatBadgeModule, MatCardModule, MatProgressBarModule, MatDialogModule, MatGridListModule, MatRippleModule, MatProgressSpinnerModule, MatStepperModule, MatTabsModule, MatRadioModule, MatSnackBarModule, MatDatepickerModule, MatNativeDateModule, MatChipsModule} from '@angular/material';

import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import { ForgotComponent } from './pages/forgot/forgot.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { MediaFilesComponent } from './pages/media-files/media-files.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { EditProfileComponent } from './pages/profile/edit-profile/edit-profile.component';
import { EditCompanyComponent } from './pages/profile/edit-company/edit-company.component';
import { ImportComponent } from './pages/import/import.component';
import { DragDropDirective} from './pages/import/drag-drop';
import { ProjectsComponent } from './pages/projects/projects.component';
import { UsersComponent } from './pages/users/users.component';
import { SuperAdminComponent } from './pages/super-admin/super-admin.component';
import { PeakfactorComponent } from './pages/peakfactor/peakfactor.component';
import { ClientuserComponent } from './pages/clientuser/clientuser.component';
import { ConfirmationDialogComponent } from './modal/confirmation-dialog/confirmation-dialog.component';
import { UpdateClientComponent } from './modal/update-client/update-client.component'

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SidebarComponent,
    ProfileComponent,
    LoginComponent,
    SignupComponent,
    ForgotComponent,
    ResetPasswordComponent,
    MediaFilesComponent,
    NotFoundComponent,
    EditProfileComponent,
    EditCompanyComponent,
    ImportComponent,
    DragDropDirective,
    ProjectsComponent,
    UsersComponent,
    SuperAdminComponent,
    PeakfactorComponent,
    ClientuserComponent,
    ConfirmationDialogComponent,
    UpdateClientComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    DataTableModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    MatPaginatorModule,
    MatSortModule,
    MatExpansionModule,
    MatInputModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatTableModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatMenuModule,
    MatBadgeModule,
    MatCardModule,
    MatProgressBarModule,
    MatDialogModule,
    MatGridListModule,
    MatRippleModule,
    MatProgressSpinnerModule,
    MatStepperModule,
    MatDialogModule,
    MatTabsModule,
    MatRadioModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatTooltipModule,
    ChartModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA3-87FjuOHBnEMji5qwNwWQCc6nnmrCf0'
    })
  ],
  exports: [
    ReactiveFormsModule,
   
  ],
  entryComponents: [
   ConfirmationDialogComponent,
   UpdateClientComponent
  ],
  providers: [{provide: HIGHCHARTS_MODULES, useFactory: () => [ more, exporting ]}],
  bootstrap: [AppComponent]
})
export class AppModule { }
