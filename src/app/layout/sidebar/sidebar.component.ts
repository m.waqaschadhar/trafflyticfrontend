import { Component, OnInit } from '@angular/core';
import { AppDataService } from 'src/app/app-data.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  layout:any;
  user: any;
  links:any 
  constructor(private appData:AppDataService) { }

  ngOnInit() {
     let users = this.appData.getUser();
     if(users.role == "superadmin"){
       this.links = [
        {path:'/projects', icon:'home', text:'Home', class:'side-link'},
        {path:'/import', icon:'backup', text:'Import Project', class:'side-link'
      },
      {path:'/factors', icon:'timeline', text:'Peak Factors', class:'side-link'},
      {path:'/users', icon:'account_circle', text:'Users', class:'side-link'}
      ];
     }
     if(users.role == "client"){
     this.links = [
        {path:'/projects', icon:'home', text:'Home', class:'side-link'},
        {path:'/import', icon:'backup', text:'Import Project', class:'side-link'
      },
      {path:'/factors', icon:'timeline', text:'Peak Factors', class:'side-link'},
      {path:'/clientusers', icon:'account_circle', text:'Users', class:'side-link'}
      ];
     }
     if(users.role == "user"){

     this.links = [
        {path:'/projects', icon:'home', text:'Home', class:'side-link'},
      
      {path:'/factors', icon:'timeline', text:'Peak Factors', class:'side-link'},
      
      ];
     }
     

    this.appLayout();
  }

  appLayout(){
    this.appData.getAppLayout().subscribe(res => this.appLayoutSuccess(res));
  }

  appLayoutSuccess(response:any){
    this.layout = response;
  }

  expandSidebar(){
    this.layout.expand = !this.layout.expand;
    this.appData.setAppLayout(this.layout);
  }

  navigateTo(currentPage:string){
    this.layout.currentPage = currentPage;
    this.appData.setAppLayout(this.layout);
  }

}
